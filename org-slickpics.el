(require 'org-special-block-extras)
(org-special-block-extras-short-names)

;;(custom-set-faces
;;  '(fringe ((t (:inherit default)))))



(org-special-block-extras-short-names)

;; [[simg:<path>|<cols>|<rows>|<x>|<y>][<link text>]]
;; Create the link for org-mode and parse the added arguments

(org-add-link-type
 "simg"
 (lambda (handle)
   (let ((ss (split-string handle "|")))

     (333/img-slice-ov      (create-image  (nth 0 ss) 'png nil :ascent 'center  :width 400 :mask 'heuristic)
			      (read (nth 1 ss))
			      (read (nth 2 ss))
     
			      (cons (read (nth 3 ss))
				    (read (nth 4 ss))))))
 (lambda (path desc backend) nil))


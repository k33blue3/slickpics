;;; 333hacks.el -- Useful hacks
;; Author: k33blue3
;;; Code:
;; Color code values for ANSI terminals
(require 'cl-lib)

;; =========================================================
;; |                  ANSI TEXT UTILITIES                  |
;; ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨

;; Background ANSI
(defvar ANSI '()
  "ANSI code lookup table.")

(setq ANSI
      '((BG ("R""41m")  ("r""101m") ("G""42m")
	    ("g""102m") ("B""44m")  ("b""104m")
	    ("M""45m")  ("m""105m") ("C""46m")
	    ("c""106m") ("X""40m")  ("x""100m")  ;; x := Black
	    ("W""47m")  ("w""107m"))
	;; Foreground ANSI
	(FG ("W""37m") ("w""97m") ("R""31m") ("r""91m")
	    ("G""32m") ("g""92m") ("B""34m") ("b""94m")
	    ("M""35m") ("m""95m") ("C""36m") ("c""96m")
	    ("X""30m") ("x""90m"))
	;; Special ANSI Attributes
	(AT ("nc" "0m") ("b"  "1m") ("ln" "4m")
	    ("-ln""24m")("rev""7m") ("reg""27m")
	    ("bl" "5m") ("it" "3m"))))

(defun 333/ansify-type (type)

  "Get the correct alist corresponding to TYPE.
TYPE := FG | BG | AT"
  (cond
   ((string= "foreground" type)
    (alist-get 'FG ANSI))
   ((string= "background" type)
    (alist-get 'BG ANSI))
   ((string= "attribute"  type)
    (alist-get 'AT ANSI))
   ((equalp t t)
    type)))

(defun 333/ansify-brightness (val color)

  "Chose the correct key of COLOR from brightness VAL."
  (cond
   ((string= val "light")
    (downcase color))
   ((string= val "bright")
    (downcase color))
   ((string= val "dark")
    (upcase color))
   ((equalp t t)
    color)))

;;
;; (fg Y... S) Where Y is a seq of colors or attributs and a S string
;; (bg X... S) Where X is a seq of colors or attributs and a S string
;; (- S)   Where S has no attribute or color
;; (+ X... S)  Where X is a seq of fg colors or attributs and a S string
;;
(defun 333/ansify-attr (attr)

  "Convert ATTR from a string into a (unfinished) key.
This to later get the ANSI code."
  (let ((code (downcase attr)))
    (cond
     ((string= code "blue")
      "b")
     ((string= code "red")
      "r")
     ((string= code "green")
      "g")
     ((string= code "cyan")
      "c")
     ((string= code "magenta")
      "m")
     ((string= code "white")
      "w")
     ((string= code "black")
      "x")
     ((string= code "no color")
      "nc")
     ((string= code "underline")
      "ul")
     ((string= code "no underline")
      "-ul")
     ((string= code "bold")
      "b")
     ((string= code "reverse")
      "rev")
     ((string= code "normal")
      "reg")
     ((string= code "blink")
      "bl")
     ((string= code "italic")
      "it")
     ((equalp t t)
      "reg"))))

(defun gget (key glist)

  "Hack around alists with strings as keys, access value of KEY in GLIST. "
  (cadr
   (assoc key glist)))

(defun 333/ansify (type bright attr str)

  "Get the ansi version of STR with ATTR of TYPE and BRIGHT.

`TYPE'   := [back|fore]ground
`BRIGHT' := light | dark
`ATTR'   := red | blue | no color | ...
`STR'    := string to format

Ex:(333/ansify \"foreground\" \"dark\" \"green\"  \"GNU\")

"
  (let* ((t-val (333/ansify-type type))

	 (c-val (333/ansify-attr attr))

	 (b-val (333/ansify-brightness bright c-val)))
  
  (string-join
     `("\e[" ,
       (gget b-val t-val)
       ,str "\e["
       ,(gget "nc"
	      (alist-get 'AT ANSI))))))

(provide '333/ansify)


(defmacro 333/ansi-dark-fg (attr str)
  "STR with dark foreground ATTR."
  (333/ansify "foreground" "dark" attr str))

(provide '333/ansi-dark-fg)


(defmacro 333/ansi-light-fg (attr str)
  "STR with light foreground ATTR."
  (333/ansify "foreground" "light" attr str))

(provide '333/ansi-light-fg)


(defmacro 333/ansi-dark-bg (attr str)
  "STR with dark background ATTR."
  (333/ansify "background" "dark" attr str))

(provide '333/ansi-dark-bg)


(defmacro 333/ansi-light-bg (attr str)
  "STR with light background ATTR."
  (333/ansify "background" "light" attr str))

(provide '333/ansi-light-bg)


(defmacro 333/ansi-fg (attr str)
  "STR with ATTR on a light foreground."
  (333/ansify "foreground" "light" attr str))

(provide '333/ansi-fg)


(defmacro 333/ansi-bold (str)
  "bold STR on a light foreground."
  (333/ansify "foreground" "light" "bold" str))

(provide '333/ansi-bold)


(defmacro 333/ansi-line (str)
  "underlined STR on a light foreground."
v  (333/ansify "foreground" "light" "underline" str))

(provide  '333/ansi-line)


(defmacro 333/ansi-blink (str)
  "blinking STR on a light foreground."
  (333/ansify "foreground" "light" "blink" str))


(provide '333/ansi-blink)

(defmacro 333/ansi-bg (attr str)
  "STR with ATTR on a light background."
  (333/ansify "background" "light" attr str))

;; =========================================================
;; |                  PRINTING UTILITIES                   |
;; ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨

(defun 333/printf (fmt &rest attr)
  "Print FMT as a formatted string with arguments ATTR"
  (print (apply 'format (cons fmt attr ))))

(provide '333/printf)


(defun 333/msgf (fmt &rest attr)
  "Send a message  FMT as a formatted string with arguments ATTR"
  (message (apply 'format (cons fmt attr ))))

(provide '333/msgf)


(defun 333/insertf (fmt &rest attr)
  "Insert FMT as a formatted string with arguments ATTR"
  (insert (apply 'format (cons fmt attr))))

(provide '333/insertf)



(defun 333/fg-text (color str &optional face) "Return a STR with foreground COLOR."
  (propertize str (or face 'font-lock-face) `(:foreground ,color)))

(provide '333/fg-text)

(defun 333/bg-text (color str  &optional face) "Return a STR with background COLOR."
  (propertize str (or face 'font-lock-face) `(:background ,color)))

(provide '333/bg-text)

(defun 333/bold-text (str  &optional face) "Return a STR with a bold face."
  (propertize str (or face 'font-lock-face) `(:weight bold)))
(provide '333/bold-text)

(defun 333/line-text (b str  &optional face) "Return a STR ."
  (propertize str (or face 'font-lock-face) `(:underline ,b)))

(provide '333/line-text)


;; =========================================================
;; |                  ABSOLUTE INSERTION                   |
;; ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨


(defun 333/pos (n)
  "Return N if over zero, else zero."
  (if (> n 0) n 0))

(defun 333/ins-at-x-y (x y text)
  "Insert string TEXT, at col X, row Y."

  (save-excursion
    (goto-char 0)

    ;; Move to correct line, or create it if it doesn’t exist.
    (forward-line y)
    (when (< (line-number-at-pos) y)
       (newline (- y (line-number-at-pos))))

    ;; Save the value of the string on the insertion position
    ;; and move to column
    (move-to-column x)
    (let* ((eol  (line-end-position)) (cpos (point))
	   (diff (- eol cpos))
	   (old-content
	    ;; Get the content which the insertion
	    ;; will overwrite
	    (if (>= diff (length text))
		(buffer-substring-no-properties
		 cpos (+ cpos diff))

	      (buffer-substring-no-properties
	       cpos (333/pos (+ cpos diff))))))
      (progn
	;; Delete the content on insert location
	(if (>= diff (length text))
            (delete-char (length text))
          (delete-char (333/pos diff)))

	;; Insert at correct coloumn
	(333/ins-at-col x text)
	old-content))))

(provide '333/ins-at-x-y)

(defun 333/ins-at-col (col &rest strings)
  "Insert, at column COL, list of STRINGS."
  (save-excursion
    (move-end-of-line nil)
    (when (< (current-column) col)
      (insert (string-utils-pad "" (- col (current-column)))))
    (move-to-column col nil)
      (let ((p (point)))
	(apply #'insert strings)
	p)))
(provide '333/ins-at-col)

(defun 333/move-ins-at-col (col &rest strings)
  "Insert, at column COL, list of STRINGS."

    (move-end-of-line nil)
    (when (< (current-column) col)
      (insert (string-utils-pad "" (- col (current-column)))))

    (move-to-column col nil)
    
    (let ((p (point)))
      (apply #'insert strings)
    p))
(provide '333/move-ins-at-col)


(defun 333/ins-at-row (line &rest strings)
  "Insert, at line LINE, list of STRINGS."
  ;;  (if (> (line-number-at-pos) line)  ()
  (save-excursion

    (forward-line line)
    (beginning-of-line)

    (let ((p (point)))
      (apply #'insert strings)
      p)))
  

(provide '333/ins-at-row)

(defun 333/move-ins-at-row (line &rest strings)
  "Insert, at line LINE, list of STRINGS."
  
  (forward-line line)
  (beginning-of-line)
  
  (let ((p (point)))
    (apply #'insert strings))
  p)
(provide '333/move-ins-at-row)


(provide '333/hacks.el)
;;; 333hacks.el ends here.

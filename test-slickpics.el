

(setq demo-path "../img/sicp.png"
     demo-width 400
     demo-img (create-image  demo-path 'png nil :ascent 'center  :width 400 :mask 'heuristic)
     demo-height (cdr (image-size demo-img))
     demo-line-height (line-pixel-height)
     demo-slice (round (/ demo-height demo-line-height)))


;;; Package --- Summary
;;
;;; Commentary:
;;


;;; Code:

(require 'cl-lib)
(require 'dash)

(load-file "../333/333hacks.el")


(defun 333/img-slice-ov (image cols rows &optional pos)
  "Display an `IMAGE' in slices to allow horizonal text/images.
Specifying the size inserted in char `COLS' and `ROWS'.
Per default it inserts the overlay at the point,however, when specified the `POS' argument,
consisting of a cons of X,Y cordinates at the beginning of insertion '(X . Y).
"
  (save-excursion
    ;; If image is a string assume it is a correctly formatted path to an png image
    ;; and create a image obj (should be avoided due to more memory usage).
    (when (stringp image)
      (setq image (create-image  image 'png nil :width 400 :mask 'heuristic)))
     ;; Ensure the image is correctly created.
    (unless (eq (car-safe image) 'image)
      (error "Invalid Image image: %s" image))
    ;; Ensure the cons expression of the X and Y cords is correct.
    (unless (and (consp pos) (numberp (car pos)) (numberp (car pos)))
      (error "Invalid Position: %s" pos))

    ;; Iterate over, either or the number of cols as a scale of the image
    (let ((x 0.0) (dx (/ 1.0001 (or cols 1)))
	  (y 0.0) (dy (/ 1.0001 (or rows 1)))				    
	  (img-str 	"                                       "))
      
      (while (< y 1.0)
	(while (< x 1.0)
	  ;; Position before insertion
	  (line-beginning-position)	  
	  (forward-line 1)
	  (333/move-ins-at-col (- (car pos) 1));;

	  ;; insert the string at insertion point, which will be constant, as X
	  ;; the return value is the position before string, and (point) the current
	  ;; position, which only properties the inserted text.
	  (setq ov (make-overlay (333/move-ins-at-col (car pos) img-str) (point)))

	  ;;	    (setq org-font-lock-extra-keywords (list '()))

	  ;; Let org-font-lock handle the displaying of the face
	  (add-to-list
	   'org-font-lock-extra-keywords
	   '((333/img-slice-ov (0  font-lock-keyword-face t))))

	  ;; Add properties to overlay
	  (overlay-put ov `display (list (list 'slice x y dx dy) image))
	  (overlay-put ov 'face 'default)
	  ;; Set the org overlay bool
	  (overlay-put ov 'org-image-overlay t)
	  ;; Removal hooks
	  (overlay-put ov 'modification-hooks
		       (list #'(lambda (&rest x) (ov-clear x))
			     #'org-display-inline-remove-overlay))
	  (setq x (+ x dx)))	  

	(setq x 0.0
	      y (+ y dy))))))
(provide '333/img-slice-ov)

(defun 333/img-remove-ovs ()
  "Remove all instances of propertized overlays in 'org-mode'."
  (font-lock-remove-keywords
   'org-mode
   '((333/img-slice-ov (0  'font-lock-keyword-face t))))

  (ov-clear 'org-image-overlay))

		  
;;;;;;;; Propertize text as an sliced image without overlays ;;;;;;;;;;;;;;;;;;;
;;(add-text-properties start end)                                             ;;
;;			   `(display ,(list (list 'slice x y dx dy) image)    ;;
;;				     rear-nonsticky (display)                 ;;
;;                                  keymap ,image-map))                       ;;
;;                                                                            ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(provide 'slickpics)
;;
;;; slickpics.el ends here
